<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostCollection;
use Illuminate\Http\Response;
use App\Post;
use Auth;

class PostController extends Controller
{
     /**
     * Post publish, paginate and the lastes 5 comments
     */
    public function postPaginate(){
        $result =  Post::with('user')->with( ['comments' => function($c){
            $c->latest()->limit(5)->get() ;
        } ])->where('is_published', 1)->paginate(10);
        
        return PostCollection::make($result);
    }

    public function all(){
        return PostCollection::make(Post::all());
    }

    public function show($id){
        $result = Post::with(['comments'])->find($id);
        if($result){
            PostResource::withoutWrapping();

            return PostResource::make($result);
        }else{
            return [
                'errors'  => [
                    'status' => 404,
                    'title'  => 'Post not found',
                    'detail' => 'Post by '.$id.' no found'
                ] 
            ];
        }
    }

    public function getPostPublished($id){
        $result = Post::with(['comments'])->find($id);
        if($result){
            if($result->is_published){
                return PostResource::make($result);
            }else{
                return [
                    'errors'  => [
                        'status' => 401,
                        'title'  => 'You do not see this post',
                        'detail' => ''
                    ] 
                ];
            }
        }else{
            return [
                'errors'  => [
                    'status' => 404,
                    'title'  => 'Post not found',
                    'detail' => 'Post by '.$id.' no found'
                ] 
            ];
        }
    }


    public function store(PostRequest $request){
        try{
            $data               =   $request->all();
            $data["user_id"]    =   Auth::user()->id;

            $post = Post::create($data);
            return PostResource::make($post);
        }catch(\Exception $e){
            return [
                'errors'  => [
                    'status' => 500,
                    'title'  => 'error save',
                    'detail' => $e->message
                ] 
            ];
        }
    }


    public function update(PostRequest $request, $id){
        try{
            $post = Post::find($id);
            if($post){
                if($post->user_id == Auth::user()->id){
                    $data               = $request->all();
                    $post->title        = $data["title"];
                    $post->content      = $data["content"];
                    $post->is_published = $data["is_published"];
                    $post->save();
                    return PostResource::make($post);
                }else{
                    return [
                        'errors'  => [
                            'status' => 401,
                            'title'  => 'You do not have permission',
                            'detail' => ''
                        ] 
                    ];
                }
            }else{
                return [
                    'errors'  => [
                        'status' => 404,
                        'title'  => 'Post not found',
                        'detail' => 'Post by '.$id.' no found'
                    ] 
                ];
            }
            
        }catch(\Exception $e){
            return [
                'errors'  => [
                    'status' => 500,
                    'title'  => 'error save',
                    'detail' => $e->message
                ] 
            ];
        }
    }

    public function destroy($id){
        $post = Post::find($id);
        if($post){
            if($post->user_id == Auth::user()->id){
                $post->delete();
                return new Response('', Response::HTTP_NO_CONTENT);
            }else{
                return [
                    'errors'  => [
                        'status' => 401,
                        'title'  => 'You do not have permission',
                        'detail' => ''
                    ] 
                ];  
            }
        }else{
            return [
                'errors'  => [
                    'status' => 404,
                    'title'  => 'Post not found',
                    'detail' => 'Post by '.$id.' no found'
                ] 
            ];
        }
    }
}
