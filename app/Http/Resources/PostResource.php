<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            
                'type'  => 'posts',
                'id'    => (string)  $this->getRouteKey(),
                'attributes'    =>  [
                    'title'     =>  $this->title,
                    'slug'      =>  $this->slug,
                    'content'   =>  $this->content,
                    'is_published'   =>  $this->is_published,
                    'created_at' => $this->created_at ? $this->created_at->getTimestamp() : null
                ],
                'relationships' => new PostRelationshipResource($this)
        ];
         
    }

   
}
