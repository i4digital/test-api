<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentResource;
use App\Http\Resources\CommentsCollection;
use Illuminate\Http\Response;
use Auth;

class CommentController extends Controller
{
    public function getCommentsByPostIdPaginate($postId){
        $result =  Comment::where('post_id', $postId)
        ->where('is_published', 1)
        ->paginate(10);
        return CommentsCollection::make($result);
    }

    public function allCommentByPostId($postId){
        $result =  Comment::where('post_id', $postId)
        ->paginate(10);
        return CommentsCollection::make($result);
    }

    public function store(Request $request){
        try{
          
            $data               =   $request->all();
            $data["user_id"]    =   Auth::user()->id;
            $comment = Comment::create($data);

            return CommentResource::make($comment);
        }catch(\Exception $e){
            return [
                'errors'  => [
                    'status' => 500,
                    'title'  => 'Error save',
                    'detail' => $e->getMessage()
                ] 
            ];
            
        }
    }

    public function update(CommentRequest $request, $id){
        try{
            $comment = Comment::find($id);
            if($comment){
                if($comment->user_id == Auth::user()->id){
                    $data                    = $request->all();
                    $comment->content        = $data["content"];
                    $comment->is_published   = $data["is_published"];
                    $comment->save();

                    return CommentResource::make($comment);
                }else{
                    return [
                        'errors'  => [
                            'status' => 401,
                            'title'  => 'Unauthorized'
                        ] 
                    ];
                }
            }else{
                return [
                    'errors'  => [
                        'status' => 404,
                        'title'  => 'Not found comment'
                    ] 
                ];
            }
            
        }catch(\Exception $e){
            return [
                'errors'  => [
                    'status' => 500,
                    'title'  => 'Error save',
                    'detail' => $e->getMessage()
                ] 
            ];
        }
    }

    public function destroy($id){
        $comment = Comment::find($id);
        if($comment){
            if($comment->user_id == Auth::user()->id){
                $comment->delete();
                return new Response('', Response::HTTP_NO_CONTENT);
            }else{
                return [
                    'errors'  => [
                        'status' => 401,
                        'title'  => 'Unauthorized'
                    ] 
                ];
            }
        }else{
            return [
                'errors'  => [
                    'status' => 404,
                    'title'  => 'Not found comment'
                ] 
            ];
        }
    }

}
