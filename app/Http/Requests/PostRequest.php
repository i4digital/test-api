<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'title'          =>  'required',
           'slug'           =>  'required',
           'content'        =>  'required',
           'is_published'   => 'required|boolean'
       ];
    }

    public function messages()
    {
        return [
           'title'          =>  'Campo obligatorio',
           'slug'           =>  'Campo obligatorio',
           'content'        =>  'Campo obligatorio',
           'is_published'   =>  'Campo obligatorio y booleano',

        ];
    }
}
