<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/**E.P. Authentication */
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signUp');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user'); // test
     });
});


/**E.P. Post */
Route::group([
    'prefix' => 'post'
], function () {
    Route::get('all-paged', 'PostController@postPaginate')->name('api.posts.index');
    Route::get('post-published/{id}', 'PostController@getPostPublished')->name('api.posts.published');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('all', 'PostController@all')->name('api.posts.all');
        Route::post('store', 'PostController@store');
        Route::patch('update/{id}', 'PostController@update');
        Route::get('{id}', 'PostController@show')->name('api.posts.show');
        Route::delete('{id}', 'PostController@destroy');

    });
});


/**E.P. Comment */
Route::group([
    'prefix' => 'comment'
], function () {
    Route::get('all-paged/{postId}', 'CommentController@getCommentsByPostIdPaginate');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('all/{postId}', 'CommentController@allCommentByPostId');
        Route::post('store', 'CommentController@store');
        Route::patch('update/{id}', 'CommentController@update');
        Route::delete('{id}', 'CommentController@destroy');

    }); 
});