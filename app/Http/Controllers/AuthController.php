<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Response;
class AuthController extends Controller
{
    /**
     * User registration
     */
    public function signUp(Request $request){
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string'
        ]);

       $user=  User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return [
            'data'=>[
                'type'  => 'users',
                'id'    => (string)  $user->id,
            ]
        ];

    }

    public function login(Request $request){
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials))
        return [
            'errors'  => [
                'status' => 401,
                'title'  => 'Unauthorized',
                'detail' => 'email or password incorrect'
            ] 
        ];

        $user = $request->user();
        $tokenResult = $user->createToken('test-api');

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
        ], 200);
    }

    public function logout(Request $request){
        $request->user()->token()->revoke();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

  
    public function user(Request $request){
        return response()->json($request->user());
    }
}
