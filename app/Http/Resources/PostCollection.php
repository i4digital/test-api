<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;
use App\Comment;
use App\User;
class PostCollection extends ResourceCollection
{
    //public $collects = PostResource::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'  =>  PostResource::collection($this->collection)
        ];
    }

    public function with($request)
    {
        $comments = $this->collection->flatMap(
            function ($post) {
                return $post->comments;
            }
        );
        $authors  = $this->collection->map(
            function ($article) {
                return $article->user;
            }
        );

        $included = $authors->merge($comments)->unique('id');

        return [
            'links'    => [
                'self' => route('api.posts.index'),
            ],
            'included' => $this->withIncluded($included),
        ];
    }

    private function withIncluded(Collection $included)
    {
        return $included->map(
            function ($include) {
                if ($include instanceof User) {
                    return new UserResource($include);
                }
                if ($include instanceof Comment) {
                    return new CommentResource($include);
                }
            }
        );
    }
}
